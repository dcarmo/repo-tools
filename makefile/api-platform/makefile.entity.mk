include .env

ifeq (entity,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY: entity-default getset-default

getset-default:
	docker-compose exec php sh -c 'cd /srv/api && php bin/console make:entity --regenerate App'

entity-default:
	docker-compose exec php sh -c 'cd /srv/api && php bin/console make:entity $(RUN_ARGS)'

# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@:
