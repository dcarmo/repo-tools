include .env

ifeq (psql,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY: dump-schema-default dump-data-default re-create-entities-from-db-default db-update-scema-from-entities-default db-default dbshell-default psql-default

dump-schema-default:
	docker-compose exec -T php sh -c 'cd /srv/api && php bin/console doctrine:schema:create --dump-sql > db-seed/schema.sql'

dump-data-default:
	rm db-seed/data.sql.gz || true
	docker-compose exec -T db pg_dump \
		--username=$(DB_USERNAME) \
		--no-owner \
		--data-only \
		$(DB_NAME) \
		> db-seed/data.sql
	gzip db-seed/data.sql

re-create-entities-from-db-default:
	docker exec php sh -c 'cd /srv/api && php bin/console doctrine:mapping:import "App\Entity" annotation --path=src/Entity'
	make getset
#	DROP TABLE IF EXISTS `__game_log_starting_2013_with_player_name`;
#    DROP TABLE IF EXISTS `__transactions_revised1`;
#    DROP TABLE IF EXISTS `tbl_new`;
#    DROP TABLE IF EXISTS `real_plus_minus`;
#    DROP TABLE IF EXISTS `real_plus_minus_id`;
#    DROP TABLE IF EXISTS `tbl_newtransactions_revised`;
#    DROP TABLE IF EXISTS `transactions`;
#    DROP TABLE IF EXISTS `transactions_revised`;
#    DROP TABLE IF EXISTS `_check_game_log_court_time`;

db-update-scema-from-entities-default:
	docker-compose exec php sh -c 'cd /srv/api && php bin/console doctrine:schema:update --force'
	make getset



db-default:
	# todo make this figure out the db type
	docker-compose exec db psql -U $(DB_USERNAME) $(DB_NAME)

dbshell-default:
	docker-compose exec db sh

psql-default:
	docker-compose exec db psql -U $(DB_USERNAME) $(DB_NAME) -c $(RUN_ARGS)

# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@:
