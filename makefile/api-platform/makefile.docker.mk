include .env

ALL_CONTAINERS=$(shell docker ps -a -q --filter="name=$(PROJECT_NAME)")

.PHONY: up-default up-debug-default kill-default down-default stop-default prune-default ps-default shell-default logs-default

kill-default:
	docker stop $(ALL_CONTAINERS)

up-default:
	make persistence
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose pull --ignore-pull-failures
	docker-compose up -d --remove-orphans
	make install
	@echo "waiting...."
	sleep 10;
	#docker-compose exec -T db psql -U ${DB_USERNAME} ${DB_NAME} -c "\dt"
	@echo "You can view the site at https://$(PROJECT_DOMAIN)"

build-default:
	docker-compose pull --ignore-pull-failures
	docker-compose -f docker-compose.build.yml build

up-debug-default:
	make down
	make persistence
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose -f docker-compose.debug.yml pull --ignore-pull-failures
	docker-compose -f docker-compose.debug.yml up -d --remove-orphans
	docker-compose exec -T php bash -c 'php -i | grep -i xdebug'
	#make install

config-default:
	docker-compose -f docker-compose.yml config

down-default: stop

stop-default:
	@echo "Stopping containers for $(PROJECT_NAME)..."
	@docker-compose -f docker-compose.yml stop

prune-default:
	@echo "Removing containers for $(PROJECT_NAME)..."
	@docker-compose -f docker-compose.yml down --remove-orphans -v

ps-default:
	@docker ps -a --filter name='$(PROJECT_NAME)*'

shell-default:
	@docker-compose exec -it php sh

logs-default:
	@docker-compose logs -f $(filter-out $@,$(MAKECMDGOALS))

# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@:
