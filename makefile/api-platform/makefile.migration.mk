include .env

.PHONY: migration-diff-default migration-default migrate-default

migration-diff-default:
	docker-compose exec php sh -c 'cd /srv/api && php bin/console make:migration:diff'

migration-default:
	docker-compose exec php sh -c 'cd /srv/api && php bin/console doctrine:cache:clear-metadata' || true
	docker-compose exec php sh -c 'cd /srv/api && php bin/console make:migration'

migrate-default:
	docker-compose exec php sh -c 'cd /srv/api && php bin/console --no-interaction doctrine:migrations:migrate && php bin/console --no-interaction doctrine:migrations:status'
# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@:
