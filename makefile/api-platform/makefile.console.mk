include .env

.PHONY: console-default

ifeq (console,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  echo $(RUN_ARGS)
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

console-default:
	docker-compose exec php sh -c 'cd /srv/api && php bin/console $(RUN_ARGS)'

# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@:
