include .env

.PHONY: composer-default update-default install-default

# If the first argument is "composer"...
ifeq (composer,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

composer-default:
	docker-compose exec php sh -c 'cd /srv/api && composer $(RUN_ARGS)'

update-default:
	docker-compose exec php sh -c 'cd /srv/api && composer update --with-dependencies'

install-default:
	docker-compose exec php sh -c 'cd /srv/api && composer install'


# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@:
