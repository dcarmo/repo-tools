include .env

.PHONY: solrstatus-default solrreindex-default solrquery-default

solrstatus-default:
	docker-compose exec -T solr wget -qO- "http://localhost:8983/solr/obd_enrolment/dataimport?command=status"

solrreindex-default:
	docker-compose exec -T solr wget -qO- "http://localhost:8983/solr/obd_enrolment/dataimport?command=full-import&jdbchost=mariadb-obd&jdbcdb=obd&jdbcuser=obd&jdbcpassword=obd"

solrquery-default:
	docker-compose exec -T solr wget -qO- "http://localhost:8983/solr/obd_enrolment/select?wt=json&indent=true&q=id:*"

# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@:
