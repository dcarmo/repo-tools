# include .env

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_DIR_PATH := $(dir $(MAKEFILE_PATH))
CURRENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MAKEFILE_PATH))))

.PHONY: setup-default install-default

setup-default:
	@echo "Setting up environment"
	@echo "Our set up requires brew to be pre-installed"
	@echo "This script will automatically download the dependancies required by this project."

	@command -v brew >/dev/null 2>&1 || { echo >&2 "I require brew but it's not installed. Aborting."; exit 1; }

	@if [ ! command -v xcodegen &> /dev/null ]; then \
		echo "xcodegen missing. Installing ..."; \
		brew install xcodegen; \
	fi

	@if [ ! command -v cocoapods &> /dev/null ]; then \
		echo "cocoapods missing. Installing ..."; \
		brew install cocoapods; \
	fi

	@echo "Setup complete! You're ready to run make install!"

install-default:
	@echo "Remove existing xcodeproj and xcworkspace to be regenerated"
	@rm -rf $(MAKEFILE_DIR_PATH)*.xcodeproj $(MAKEFILE_DIR_PATH)*.xcworkspace

	@echo "xcodegen - Generating xcodeproj"
	@xcodegen

	@echo "pod install - Installing dependencies"
	@pod install