env-default:
ifndef yml_path
	@echo "No yml_path specified, defaulting to '.gitlab-ci.yml'"
	$(eval yml_path = .gitlab-ci.yml)
endif

ifndef variables_path
	@echo "No variables_path specified, defaulting to 'local'"
	$(eval variables_path = local)
endif

	@echo "Loading environment from the $(yml_path) file for $(yml_path)"
	@.repo-tools/lib/create_env.sh $(yml_path) $(variables_path)
