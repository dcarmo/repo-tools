include .env

persistence-default:
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/db || true;
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/log || true;
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/files || true;
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/files_private || true;
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/solr || true;
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/cache || true;

purge-persistence-default: stop
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/db || true;
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/log || true;
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/files || true;
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/files_private || true;
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/solr || true;
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/cache || true;

purge-db-default: stop
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/db || true;

purge-logs-default: stop
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/log || true;

purge-files-default: stop
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/files || true;
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/files_private || true;

purge-solr-default: stop
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/solr || true;

purge-cache-default: stop
	rm -rf ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/cache || true;
