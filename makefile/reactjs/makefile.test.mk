# file will run relative to project root.

include .env

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_DIR_PATH := $(dir $(MAKEFILE_PATH))
CURRENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MAKEFILE_PATH))))

#ZSH_TOOLS_RESULT := $(shell if [ ! -f tools/test.sh ]; then echo "Tools not installed"; fi;)

.PHONY: up-default down-default stop-default prune-default ps-default shell-default logs

test-default:
ifndef test
	docker exec $(PROJECT_NAME)_php_$(CI_COMMIT_REF_SLUG) bash -c 'cd /var/www/html && ./vendor/bin/phpunit --testdox ./test/'
else
	docker exec $(PROJECT_NAME)_php_$(CI_COMMIT_REF_SLUG) bash -c 'cd /var/www/html && ./vendor/bin/phpunit --testdox ./test/$(test)*'
endif

test-rebuild-default: up
	sleep 3
	test


# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@: