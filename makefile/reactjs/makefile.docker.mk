# file will run relative to project root.

include .env

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_DIR_PATH := $(dir $(MAKEFILE_PATH))
CURRENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MAKEFILE_PATH))))

.PHONY: up-default down-default stop-default prune-default ps-default shell-default logs-default clean-default

up-default:
	make stop
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/logs || true;
	mkdir -p ${CI_SERVER_PERSISTENT_STORAGE_LOCATION_ROOT}/${PROJECT_NAME}/files || true;
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose build
	docker-compose pull --ignore-pull-failures
	docker-compose up -d --remove-orphans

up-verbose-default:
	make stop
	@echo "Starting up containers for $(PROJECT_NAME)..."
	docker-compose build
	docker-compose up --remove-orphans

down-default: stop

stop-default:
	@echo "Stopping containers for $(PROJECT_NAME)..."
	@docker-compose stop

prune-default:
	@echo "Removing containers for $(PROJECT_NAME)..."
	@docker-compose down -v

ps-default:
	@docker ps --filter name='$(PROJECT_NAME)*'

shell-default:
	docker exec -ti -e COLUMNS=$(shell tput cols) -e LINES=$(shell tput lines) $(shell docker ps --filter name='$(PROJECT_NAME)_php_$(CI_COMMIT_REF_SLUG)' --format "{{ .ID }}") sh

logs-default:
	@docker-compose logs -f $(filter-out $@,$(MAKECMDGOALS))

clean-default:
	find src/vendor -type d -name \.git -exec rm -rf \{\} \;

# https://stackoverflow.com/a/6273809/1826109
%: %-default
	@: