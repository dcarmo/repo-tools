#!/usr/bin/env bash
# shellcheck disable=SC1003

# Based on https://gist.github.com/pkuczynski/8665367

YML_FILE=${1:-.gitlab-ci.yml}
VARIABLES_PATH=${2:-local}

echo "Looking in yml file for ${VARIABLES_PATH}"

parse_yaml() {
    local yaml_file=$1
    local variables_path=$2
    local s
    local w
    local fs

    s='[[:space:]]*'
    w='[a-zA-Z0-9_.-]*'
    fs="$(echo @|tr @ '\034')"

    (
        sed -e '/- [^\“]'"[^\']"'.*: /s|\([ ]*\)- \([[:space:]]*\)|\1-\'$'\n''  \1\2|g' |

        sed -ne '/^--/s|--||g; s|\"|\\\"|g; s/[[:space:]]*$//g;' \
            -e "/#.*[\"\']/!s| #.*||g; /^#/s|#.*||g;" \
            -e "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
            -e "s|^\($s\)\($w\)${s}[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" |

        awk -v awk_yaml_file=${yaml_file} -v variables_path=${VARIABLES_PATH} -F"$fs" '{
            indent = length($1)/2;
            if (length($2) == 0) { conj[indent]="+";} else {conj[indent]="";}
            vname[indent] = $2;
            gbl=false;
            lcl=false;
            for (i in vname) {if (i > indent) {delete vname[i]}}
                if (length($3) > 0) {
                    vn="";
                    for (i=0; i<indent; i++) {
                        vn=(vn)(vname[i])("_")
                    }
                    if (vn == variables_path "_variables_") {
                       if (lcl == false) {
                         print("\n# " variables_path " Override variable from " awk_yaml_file ":");
                         lcl = true;
                       }
                       printf("%s%s=%s\n", $2, conj[indent-1],$3);
                    }
                    if (vn == "variables_") {
                       if (gbl == false) {
                         print("\n# Global variable from .gitlab-ci.yml:");
                         gbl = true;
                       }
                       printf("%s%s=%s\n", $2, conj[indent-1],$3);
                    }
                }
            }' |

        sed -e 's/_=/+=/g' |

        awk 'BEGIN {
                FS="=";
                OFS="="
            }
            /(-|\.).*=/ {
                gsub("-|\\.", "_", $1)
            }
            { print }'
    ) < "$yaml_file"
}

create_variables() {
    local yaml_file="$1"
    eval "$(parse_yaml "$yaml_file")"
}

#if grep -q "\.env generated by makefile" .env;
#    then
#        echo ".env already contains generated content! Skipping"
#        cat .env
#	else

touch .env

echo '###############################################################################' > .env
echo '#' >> .env
echo '#  __________                       ___________           .__' >> .env
echo '#  \______   \ ____ ______   ____   \__    ___/___   ____ |  |   ______' >> .env
echo '#   |       _// __ \\____ \ /  _ \    |    | /  _ \ /  _ \|  |  /  ___/' >> .env
echo '#   |    |   \  ___/|  |_> >  <_> )   |    |(  <_> |  <_> )  |__\___ \' >> .env
echo '#   |____|_  /\___  >   __/ \____/    |____| \____/ \____/|____/____  >' >> .env
echo '#          \/     \/|__|                                            \/' >> .env
echo '#' >> .env
echo '# This file is auto generated DO NOT EDIT. To make changes, update ' ${YML_FILE} >> .env
echo '# and re-run `make env`.' >> .env
echo '#' >> .env
echo '# See: https://gitlab.com/dcarmo/repo-tools' >> .env
echo '###############################################################################' >> .env
echo '' >> .env
echo '' >> .env
echo '# .env generated by `make env`' >> .env
echo '#     ____   ____     _   __ ____  ______   __  ___ ____   ____   ____ ________  __' >> .env
echo '#    / __ \ / __ \   / | / // __ \/_  __/  /  |/  // __ \ / __ \ /  _// ____/\ \/ /' >> .env
echo '#   / / / // / / /  /  |/ // / / / / /    / /|_/ // / / // / / / / / / /_     \  / ' >> .env
echo '#  / /_/ // /_/ /  / /|  // /_/ / / /    / /  / // /_/ // /_/ /_/ / / __/     / /  ' >> .env
echo '# /_____/ \____/  /_/ |_/ \____/ /_/    /_/  /_/ \____//_____//___//_/       /_/   ' >> .env
echo '#                                                                                  ' >> .env
echo '# To modify this file, edit the ' ${YML_FILE} ' file and then re-run `make env`' >> .env
echo '' >> .env
echo '' >> .env
parse_yaml ${YML_FILE} ${VARIABLES_PATH} | sed 's/\\"//g' >> .env
echo '' >> .env
echo '' >> .env
echo '# .env generated by `make env`' >> .env
echo '#     ____   ____     _   __ ____  ______   __  ___ ____   ____   ____ ________  __' >> .env
echo '#    / __ \ / __ \   / | / // __ \/_  __/  /  |/  // __ \ / __ \ /  _// ____/\ \/ /' >> .env
echo '#   / / / // / / /  /  |/ // / / / / /    / /|_/ // / / // / / / / / / /_     \  / ' >> .env
echo '#  / /_/ // /_/ /  / /|  // /_/ / / /    / /  / // /_/ // /_/ /_/ / / __/     / /  ' >> .env
echo '# /_____/ \____/  /_/ |_/ \____/ /_/    /_/  /_/ \____//_____//___//_/       /_/   ' >> .env
echo '#                                                                                  ' >> .env
echo '# To modify this file, edit the ' ${YML_FILE} ' file and then re-run `make env`' >> .env
echo '' >> .env
echo '' >> .env

echo "Created .env:"
cat .env
#fi