###############################################################################
#
#  __________                       ___________           .__
#  \______   \ ____ ______   ____   \__    ___/___   ____ |  |   ______
#   |       _// __ \\____ \ /  _ \    |    | /  _ \ /  _ \|  |  /  ___/
#   |    |   \  ___/|  |_> >  <_> )   |    |(  <_> |  <_> )  |__\___ \
#   |____|_  /\___  >   __/ \____/    |____| \____/ \____/|____/____  >
#          \/     \/|__|                                            \/
#
# This file is the common base for the repo tools. It wil install the tools to
# the .repo-tools/ directory when you run `make` or `make install-tools`.
#
# See: https://gitlab.com/dcarmo/repo-tools
#
# Specify your project type here:

# Available PROJECT_TYPES:
# api-platform
# reactjs
# xcode
PROJECT_TYPE:=reactjs

#
###############################################################################

# Specify this so that it'll run properly on ubuntu
# https://stackoverflow.com/a/12230723
SHELL := /bin/bash

# Repo Tools installation
.DEFAULT_GOAL := install-tools

install-tools:
	@if [[ ! -a .repo-tools ]]; then \
		touch .env; \
			echo "Installing tools..."; \
		git clone --depth 1 --branch 0.0.1 https://gitlab.com/dcarmo/repo-tools.git .repo-tools; \
	else \
		echo "Tools already installed. To update delete the .repo-tools folder and update the branch version to download."; \
	fi;

help:
	@echo "Your current project is configured to use the '$(PROJECT_TYPE)' template."
	@cat .repo-tools/README.md
	@echo ''
	@cat .repo-tools/common/README.md
	@echo ''
	@cat .repo-tools/$(PROJECT_TYPE)/README.md
	@echo ''

# Include common make files that are not specific to a particular project.
COMMON_INCLUDES:=$(wildcard .repo-tools/makefile/common/makefile.*.mk)
ifneq ($(strip $(COMMON_INCLUDES)),)
  contents :=  $(shell echo including extra rules $(COMMON_INCLUDES))
  include $(COMMON_INCLUDES)
endif

# Include project type make files.
PROJECT_INCLUDES:=$(wildcard .repo-tools/makefile/$(PROJECT_TYPE)/makefile.*.mk)
ifneq ($(strip $(PROJECT_INCLUDES)),)
  contents :=  $(shell echo including extra rules $(PROJECT_INCLUDES))
  include $(PROJECT_INCLUDES)
endif

###############################################################################
#
#  If you need to create custom make commands for this specific project
#  you can create `makefile.*.mk` files and add items there.
#
###############################################################################

EXTRA_INCLUDES:=$(wildcard makefile.*.mk)
ifneq ($(strip $(EXTRA_INCLUDES)),)
  contents :=  $(shell echo including extra rules $(EXTRA_INCLUDES))
  include $(EXTRA_INCLUDES)
endif
